from dataclasses import (dataclass, field)
from typing import List
from input_values import deeps
import time 

@dataclass
class DeepMeasurement:
    start: int
    counter: int = 0
    deltas: List[int] = field(default_factory=list)
    
    size_w: int = 3
    counter_w: int = 0
    window: List[int] = field(default_factory=list)
    window_deltas: List[int] = field(default_factory=list)

    def measure(self, value):
        delta = value - self.start
        if delta > 0:
            self.counter += 1
        self.deltas.append((value, delta))
        self.start = value
        # check window deltas
        self.window.append(delta)
        if len(self.window) > self.size_w:
            del self.window[0]
        if len(self.window) == 3:
            total = sum(self.window)
            if total > 0:
                self.counter_w += 1
            self.window_deltas.append((value, total))
            


    def description(self):
        for value, delta in self.deltas:
            if delta > 0:
                print(value, "(increased)")
            elif delta < 0:
                print(value, "(decreased)")
            else:
                print(value, "(nothing)")

    def description_window(self):
        for value, delta in self.window_deltas:
            if delta > 0:
                print(value, delta, "(increased)")
            elif delta < 0:
                print(value, delta, "(decreased)")
            else:
                print(value, delta, "(nothing)")



if __name__ == '__main__':
    # values = [200, 208, 210, 200, 207, 240, 267, 260, 263]
    dm = DeepMeasurement(start=199)
    for value in deeps:
        dm.measure(value)
    # dm.description()
    # print("Total of increased measurementent", dm.counter)
    # # convert list given to python list
    # # sed -i 's/[0-9]$/&,/g' input_values.py
    # print("Total of increased measurementent by window")
    # dm.description_window()
    print("Total of increased measurementent", dm.counter_w)
