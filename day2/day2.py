from dataclasses import (dataclass, field)
from typing import List
from input_mov import moves
import time 

@dataclass
class DeepMeasurement:
    horizontal: int = 0
    start: int = 0
    counter: int = 0
    deltas: List[int] = field(default_factory=list)
    
    size_w: int = 3
    counter_w: int = 0
    window: List[int] = field(default_factory=list)
    window_deltas: List[int] = field(default_factory=list)


    aim: int = 0

    def measure(self, value):
        delta = value - self.start
        if delta > 0:
            self.counter += 1
        self.deltas.append((value, delta))
        self.start = value
        # check window deltas
        self.window.append(delta)
        if len(self.window) > self.size_w:
            del self.window[0]
        if len(self.window) == self.size_w:
            total = sum(self.window)
            if total > 0:
                self.counter_w += 1
            self.window_deltas.append((value, total))
            


    def description(self):
        for value, delta in self.deltas:
            if delta > 0:
                print(value, "(increased)")
            elif delta < 0:
                print(value, "(decreased)")
            else:
                print(value, "(nothing)")

    def description_window(self):
        for value, delta in self.window_deltas:
            if delta > 0:
                print(value, delta, "(increased)")
            elif delta < 0:
                print(value, delta, "(decreased)")
            else:
                print(value, delta, "(nothing)")

    def forward(self, value):
        self.horizontal += value
        self.vertical(self.aim*value)

    def vertical(self, value):
        new_value = self.start + value
        self.measure(new_value)

    def vertical_aim(self, value):
        #new_value = self.start + value
        #self.measure(new_value)
        self.aim += value

    def command(self, order):
        opts = {
            "forward": self.forward,
            "down": self.vertical_aim,
            "up": lambda e: self.vertical_aim(-e)
        }
        word, value = order.split()

        fn = opts.get(word, print)
        if value.isdigit():
            fn(int(value))
            print(order, self.position, self.aim)

    @property
    def position(self):
        return (self.horizontal, self.start)

    @property
    def multiplication(self):
        return self.horizontal * self.start

if __name__ == '__main__':
    test_moves = [
        "forward 5",
        "down 5",
        "forward 8",
        "up 3",
        "down 8",
        "forward 2"
    ]
    dm = DeepMeasurement()

    for value in moves:
        dm.command(value)

    print("Final position", dm.position)
    print("Final position", dm.multiplication)
