from dataclasses import (dataclass, field)
from typing import List, Dict
from values import bins
import re 

def counter_factory(n):
    return dict.fromkeys([str(i) for i in range(n)], 0)


def calculate_oxigen(counter, steps, n):
    if len(steps) == 1:
        result = steps.pop()
        return "".join(result)
    else:
        v = counter[str(n)]
        if v >= len(steps)/2:
            # filter all values with 1
            steps = list(filter(lambda item: item[n]=="1", steps))
        else:
            steps = list(filter(lambda item: item[n]=="0", steps))

        counter = counter_factory(len(steps[0]))
        for item in steps:
            for i, v in enumerate(item):
                counter[str(i)] += int(v)
        return calculate_oxigen(counter, steps, n+1)
        
def calculate_co2(counter, steps, n):
    if len(steps) == 1:
        result = steps.pop()
        return "".join(result)
    else:
        v = counter[str(n)]
        if v >= len(steps)/2:
            # filter all values with 1
            steps = list(filter(lambda item: item[n]=="0", steps))
        else:
            steps = list(filter(lambda item: item[n]=="1", steps))

        counter = counter_factory(len(steps[0]))
        for item in steps:
            for i, v in enumerate(item):
                counter[str(i)] += int(v)
        return calculate_co2(counter, steps, n+1)
        



class PowerMonitor:

    def __init__(self, n):
        self.regex = regex = re.compile("^[01]{%s}$" % n)
        print(self.regex)
        self.step = []
        self.counter = counter_factory(n)
        

    def add_value(self, value):
        if self.regex.match(value):
            item = list(value)
            for i, v in enumerate(item):
                self.counter[str(i)] += int(v)
            self.step.append(list(value))

    @property
    def len(self):
        return len(self.step)

    @property
    def gamma(self):
        result = ""
        for i, v in self.counter.items():
            if v >= self.len/2:
                result += "1"
            else:
                result += "0"
        return result

    @property
    def epsilon(self):
        result = ""
        for i, v in self.counter.items():
            if v >= self.len/2:
                result += "0"
            else:
                result += "1"
        return result

    @property
    def power(self):
        return int(self.gamma , 2) * int(self.epsilon ,2)


    @property
    def oxigen(self):
        counter = self.counter
        selection = self.step
        result = calculate_oxigen(counter, selection, 0)
        return int(result,2)
        

    @property
    def co2(self):
        counter = self.counter
        selection = self.step
        result = calculate_co2(counter, selection, 0)
        return int(result,2)
        
    @property
    def life_support(self):
        return self.oxigen * self.co2


if __name__ == '__main__':
    # sed -i 's/^[10]/\"&/g;s/[01]$/&\",/g' values.py
    test_values = [
       "00100",
       "11110",
       "10110",
       "10111",
       "10101",
       "01111",
       "00111",
       "11100",
       "10000",
       "11001",
       "00010",
       "01010",
    ]
    n = len(bins[0])
    print(n)
    dm = PowerMonitor(n)
    for value in bins:
        dm.add_value(value)

    print("Final value", dm.gamma, dm.epsilon, dm.power)
    print("Oxigen", dm.oxigen)
    print("CO2", dm.co2)
    print("Life support", dm.life_support)
