"""
https://adventofcode.com/2021/day/4

Calamar Gigante.

Profundida: 1.5km bajo el mar. 
No se ve luz 
Lo que se puede ver -> calamar 

Jugar bingo 

Board -> 5 x 5 con numeros

Numeros son random.

El número elegido se marca en todas las tarjetas

Si toda la fila o tota columna tiene marcas, entonces gana.

"""
import random
from rich import print 
import time
from pathlib import Path

def factory_line(n, gen_val, unique_set, i, board_values, *args):
    return [gen_val(i, j, unique_set, board_values) for j in range(n)]

def zero(*args):
    return 0

def aleatorio(i, j, unique_set,*args):
    while (val:=random.randint(0, 99)) in unique_set.keys():
        pass
    unique_set[val] = (i, j)
    return val
    

def fixed_board(i, j, unique_set, board_values,*args):
    value = board_values[i][j]
    unique_set[value] = (i, j)
    return value


class Board:
    """
    2 matrix
    1 with the random numbers 
    1 with marks
    1 dict with numbers as keys and position tuples as v
    """
    size:int = 5

    def __init__(self, idb, board_values, gen):
        self.idb = idb
        self.set_values = dict()
        self.values = [factory_line(self.size, gen,
                                    self.set_values, i, board_values) for i in range(self.size)]
        self.counter = [factory_line(self.size, zero, self.set_values, i, board_values) for i in
                        range(self.size)]

    def is_bingo(self, n, value):
        bingo_row = False 
        bingo_col = False
        total = 0

        if (pos:=self.set_values.get(value)):
            i, j = pos
            self.counter[i][j] = 1 
            # check row 
            
            row = self.counter[i]
            if sum(row) == self.size:
                bingo_row = True
            # check column
            column = [row[j] for row in self.counter]

            if sum(column) == self.size:
                bingo_col = True
                val_column = [row[j]  for row in self.values]


            if bingo_col or bingo_row:
                for i, row in enumerate(self.counter):
                    for j, val in enumerate(row):
                        if val == 0:
                            total += self.values[i][j]

        return (bingo_row, bingo_col), {"score": total * value,
                                        "total": total, "value":
                                        value,"pos":n}

    def bingo(self, value):
        return any(self.bingo(value))

    def __hash__(self):
        sumtot = sum([sum(r) for r in self.values])
        return hash((self.idb, sumtot))


def read_input(path="board.input"):
    stage = "NUMBERS"
    boards = {0: []}
    counter = 0
    sub_counter = 0
    numbers = []
    if Path(path).exists():
        with open(path) as f:
            for line in f.readlines():
                if line != "\n" and stage == "NUMBERS": 
                    numbers += list(
                        map(lambda e:int(e.strip()), line.split(",")))
                    continue
                if line == "\n" and stage == "NUMBERS": 
                    stage = "BOARDS"
                    continue
                if stage == "BOARDS" and line != "\n":
                    new_values = list(
                        map(lambda e:int(e.strip()), line.strip().split()
                            ))
                    boards[counter].append(new_values)
                    sub_counter += 1
                    continue
                if stage == "BOARDS" and line == "\n":
                    counter += 1
                    sub_counter = 0
                    boards[counter] = []
                    continue
    return numbers, boards


if __name__ == '__main__': 
    def run():
        numbers, boards = read_input("board.input")
        participants = {}
        winners = {}
        last = None
        group = set()

        for i, v in boards.items():
            participants[i] = Board(i, v, fixed_board)

        for n, number in enumerate(numbers):
            for i, v in boards.items():
                p = participants[i]
                bingo, score = p.is_bingo(n, number)

                if any(bingo):                    
                    if p not in group:
                        if not winners:
                            first = p.idb
                        winners[p.idb] = (p, number, score)
                        group.add(p)
                        last = p.idb
                        
        return winners, first, last

    winners, first, last = run()

    first = winners.get(first)
    print(first)

    last = winners.get(last)
    print(last)
