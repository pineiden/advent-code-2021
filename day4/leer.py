from pathlib import Path

def read_input(path="board.input"):
    stage = "NUMBERS"
    boards = {0:[]}
    counter = 0
    sub_counter = 0
    if Path(path).exists():
        with open(path) as f:
            for line in f.readlines():
                if line != "\n" and stage == "NUMBERS": 
                    numbers += list(
                        map(lambda e:int(e.strip()), line.split(",")))
                    continue
                if line == "\n" and stage == "NUMBERS": 
                    stage = "BOARDS"
                    continue
                if stage == "BOARDS" and line != "\n":
                    boards[counter][sub_counter] = list(
                        map(lambda e:int(e.strip()), line.split()
                            )
                    sub_counter += 1
                    continue
                if stage == "BOARDS" and line == "\n":
                    counter += 1
                    sub_counter = 0
                    boards[counter] = []
                    continue
    return numbers, boards
