import random
from rich import print 
import time
from pathlib import Path
from dataclasses import (dataclass, field)
from math import copysign 

@dataclass
class Point:
    x:int 
    y:int

    @classmethod
    def parse(cls, value):
        position = tuple(map(int, value.split(",")))
        return cls(*position)

    def __hash__(self):
        return hash((self.x, self.y))


def check_angle_45(p1, p2):
    """
    check for angle 45
    """
    a = p2.x-p1.x
    b = p2.y-p1.y
    orientation = tuple(copysign(-1.0,e) for e in (a,b))

    if abs(a) == abs(b):
        return True, orientation
    else:
        return False, orientation

@dataclass
class Vector:
    p1: Point 
    p2: Point

    @classmethod
    def parse(cls, value):
        points = tuple(map(lambda e:Point.parse(e.strip()), value.split("->")))
        return cls(*points)

    @property
    def coverage(self):
        """
        Just for horizontal and vertical vectors
        """
        is45, orientation = check_angle_45(self.p1, self.p2)
        if self.p1.x == self.p2.x and self.p1.y != self.p2.y:
            p_min = min([self.p1.y, self.p2.y])
            p_max = max([self.p1.y, self.p2.y])

            return [Point(self.p1.x, v) for v in range(p_min, p_max+1)]

        elif self.p1.y == self.p2.y and self.p1.x != self.p2.x:

            p_min = min([self.p1.x, self.p2.x])
            p_max = max([self.p1.x, self.p2.x])

            return [Point(v, self.p1.y) for v in range(p_min, p_max+1)]
        elif is45:
            segun_x, segun_y = orientation

            delta = abs(self.p2.x - self.p1.x)
            return [Point(self.p1.x + segun_x*v, self.p1.y + segun_y*v) for v in range(0,delta+1)]
            

        return []

    @property
    def max_x_y(self):
        return (max(self.p1.x, self.p2.x), max(self.p1.y, self.p2.y))


def read():
    points = {}
    max_x, max_y = (0, 0)
    with open("input.txt", "r") as f:
        for n, line in enumerate(f.readlines()):
            line = line.strip()
            if len(line)>0:
                try:
                    vector = Vector.parse(line)
                    mx, my = vector.max_x_y 
                    if mx > max_x:
                        max_x = mx 
                    if my > max_y:
                        max_y = my
                    cov = vector.coverage
                    for point in cov:
                        if point not in points:
                            points[point] = 0
                        points[point] += 1
                except Exception as e:
                    print("Error en", e, n, line)
                    raise e
    return points, (max_x, max_y)

if __name__ == '__main__':
    points, (max_x, max_y) = read()
    for j in range(0, max_x + 1):# x are coluuns
        row = [points.get(Point(i, j), 0) for i in range(0, max_y + 1)
               ]
    count = len(list(filter(lambda e:e>=2, points.values())))
    print(count)
