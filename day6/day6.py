import random
from rich import print 
import time
from pathlib import Path
from dataclasses import (dataclass, field)
from math import copysign 
import numpy as np
"""
https://adventofcode.com/2021/day/6


Modelar crecimiento de 'lanternfish'

cada una crea una nueva cada 7 días
algunas demoran 2 hasta 4 dias...
Modelar cada pez como un número que represente el numero de dias hasta
que crea un nuevo pez
"""

def can_create(timer):
    return True if timer==0 else False

@dataclass
class LanterFish:
    timer: int
    child_timer: int = 8
    iteration: int = 0

    def new_day(self):
        # check if create
        if self.iteration == 1 and can_create(self.timer):
            new = LanterFish.new(
                self.child_timer)
            self.iteration = 0
            self.child_timer = 8
            self.timer = 6
            return new

        if self.timer > 0:
            self.timer -= 1

            if self.timer == 0:
                self.iteration = 1
            if self.iteration >= 1 and self.timer > 0:
                if self.child_timer > 0:
                    self.child_timer -= 1
                elif self.child_timer == 0:
                    self.child_timer = 8
            return None

        if self.timer == 0:
            self.timer = 6
            self.iteration = 1
        return None

    @classmethod
    def new(cls, child_timer):
        return cls(timer=child_timer)

    def __repr__(self):
        return "LanterFish(%s, %s, %s)" % (
            self.timer,
            self.iteration,
            self.child_timer)
    def __str__(self):
        return str(self.timer)

def read():
    with open("input.txt", "r") as f:
        values = []
        for line in f.readlines():
            values += [int(e.strip()) for e in
                       line.strip().split(",")]
        return values

def calculate(fishes, days=256):
    total = []
    collection = []
    for day in range(0, days):
        calc = [fish.new_day() for fish in fishes] 
        news = list(filter(lambda e: e, calc))
        if news:
            fishes += news
    mapping = {i: len([fish for fish in fishes if fish.timer==i]) for
               i in range(9)}
    return fishes, mapping


def np_calculate(parsed_data, days=256):
    fishies = parsed_data.copy()

    for day in range(days):
        fishies = np.roll(fishies, -1)
        # the amount of new fishes
        # must be load over position 6 because last day were 0
        # and last column represent new fishes
        fishies[6] += fishies[-1]      

    return np.sum(fishies)

from multiprocessing import Pool, Process

if __name__ == '__main__':
    #start = [3, 4, 3, 1, 2]

    start = read()
    # day_0
    fishes = [LanterFish.new(day) for day in start]
    total = {}
    tasks = []

    results_A = {9:{}, 40:{}, 160:{}, 18: {}, 256: {}, 80:{}, 36:{}}
    cases = range(9)

    days = 9
    for case in cases:
        fishes, mapping = calculate([LanterFish.new(case)], days)
        results_A[days][case] = len(fishes)

    total = [results_A[days][case] for case in start]
    print("Total 9 days", sum(total))


    days = 18
    for case in cases:
        fishes, mapping = calculate([LanterFish.new(case)], days)
        results_A[days][case] = len(fishes)

    total = [results_A[days][case] for case in start]
    print("Total 18 days", sum(total))


    days = 36
    for case in cases:
        fishes, mapping = calculate([LanterFish.new(case)], days)
        results_A[days][case] = len(fishes)


    total = [results_A[days][case] for case in start]
    print("Total 36 days", sum(total))
    total_36 = total


    days = 256
    fishtogram = np.histogram(start,
                              bins=9, range=(0, 9))[0]
    
    print("NP calc",np_calculate(fishtogram, days))
    # days = 256
