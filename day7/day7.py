"""
A giant whale has decided your submarine is its next meal, 
and it's much faster than you are. There's nowhere to run!

Swarm of crabs rescue, everyone with a little submarina

Crabs's submarines needs to be aligned

List horizontal position: 

16,1,2,0,4,2,7,1,2,14

Every number is a crab's position 

Each change of 1 step costs 1 fuel

Objetivo: alinear

Sacar de 0 a N  y sumar las diferencias

N <= max(lista)

"""
def read():
    with open("input.txt", "r") as f:
        values = []
        for line in f.readlines():
            values += [int(e.strip()) for e in
                       line.strip().split(",")]
        return values

def move(lista, destiny):
    result = [abs(i-destiny) for i in lista]
    return sum(result)

def analize(lista):
    low = min(lista)
    up = max(lista)
    results = {i: move(lista, i) for i in range(low, up+1)}
    print(results)
    n = []
    result = min(results.values())
    for i, value in results.items():
        if value == result:
            n.append(i)
    return n, result       


if __name__ == '__main__':
    lista  = read()
    #lista = [16,1,2,0,4,2,7,1,2,14]
    analisis = analize(lista)
    print("Menor costo", analisis)
